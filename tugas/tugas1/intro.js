import React from 'react';
import {
  View,
  Text,
} from 'react-native';

const Intro = () => {
  return (
    <View style={{
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
    }}>
    <Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
    </View>
  );
};

export default Intro;
